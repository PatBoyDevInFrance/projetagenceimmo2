<?php

namespace App\DataFixtures;

use App\Entity\Property;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{   
    
    public function load(ObjectManager $manager)
    {
        
        $faker = \Faker\Factory::create('fr_FR');
        for($i = 0 ; $i < 100 ; $i++){
            $property = new Property();
            $property
            ->setTitle($faker->words(3, true))
            ->setDescription($faker->sentences(3,$asText= true))
            ->setSurface($faker->numberBetween(20,350))
            ->setRooms($faker->numberBetween(2,10))
            ->setBedRooms($faker->numberBetween(1,9))
            ->setFloor($faker->numberBetween(0,15))
            ->setPrice($faker->numberBetween(100000, 10000000))
            ->setHeat($faker->numberBetween(0, count(Property::HEAT) - 2))
            ->setCity($faker->city)
            ->setAdress($faker->address)
            ->setPostalCode($faker->postcode)
            ->setSold(false);
        $manager->persist($property);
        }
        
        

        $manager->flush();
    
   
    }
}
